import '../styles/main.scss';

import {meter} from '$helpers/meter.helper';
import {Iterations} from '$experiments/index';
import {Iterations2} from '$experiments/index';

const iterations = new Iterations();

const methods = Object.getOwnPropertyDescriptors(Object.getPrototypeOf(iterations));

for (const method in methods) {
    if (method !== 'constructor') {
       const result =  meter<void>(iterations[method], {count: iterations.meterCount, context: iterations})();
    }
}

const iterations2 = new Iterations2();

const methods2 = Object.getOwnPropertyDescriptors(Object.getPrototypeOf(iterations2));

for (const method in methods2) {
    if (method !== 'constructor') {
       const result2 =  meter<void>(iterations2[method], {count: iterations2.meterCount, context: iterations2})('0.0.0.0');
    }
}

import {Bootstrap} from './modules/bootstrap';
const bootstrap = new Bootstrap ();
