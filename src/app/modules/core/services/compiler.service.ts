
import {ReaderService} from '$reader/services';
import {SettingsService} from '$settings/services';
import {StatusbarService} from '$statusbar/services';
import {CatalogService} from '$catalog/services';
import {AbstractService} from '$helpers/index';
import {IServiceParams} from '$interfaces';


export class CompilerService extends AbstractService {
    public readonly serviceParams: IServiceParams = {
        name: 'CompilerService',
        inject: [
            'Catalog.CatalogService',
            'Reader.ReaderService',
            'Settings.SettingsService',
            'Statusbar.StatusbarService',
        ],
    }

    protected catalogService: CatalogService;
    protected readerService: ReaderService;
    protected settingsService: SettingsService;
    protected statusbarService: StatusbarService;

    constructor() {
        super();
    }
    public init(bootstrap: any): void {
        super.init(bootstrap);
    }
}
