import {AbstractService} from '$helpers/index';
import {IServiceParams} from '$interfaces';

export class CatalogService extends AbstractService{
    public readonly serviceParams: IServiceParams = {
        name: 'CatalogService',
        inject: [],
    }

    constructor() {
        super();
    }
}
