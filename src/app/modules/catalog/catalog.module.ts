import {AbstractModule} from '$helpers/index';
import {CatalogService} from '$catalog/services';
export class CatalogModule extends AbstractModule {
    public readonly moduleParams = {
        name: 'Catalog',
        components: [],
        providers: [
    CatalogService,
        ],
    }

    constructor() {
        super();
    }
}
