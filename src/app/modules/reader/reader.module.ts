import {AbstractModule} from '$helpers/index';

import {ReaderService} from '$reader/services';

export class ReaderModule extends AbstractModule {
    public readonly moduleParams = {
        name: 'Reader',
        components: [],
        providers: [
            ReaderService,
        ],
    }

    constructor() {
        super();
    }
}
