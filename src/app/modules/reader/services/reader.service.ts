import {AbstractService} from '$helpers/index';
import {IServiceParams} from '$interfaces';

export class ReaderService extends AbstractService{
    public readonly serviceParams: IServiceParams = {
        name: 'ReaderService',
        inject: [],
    }

    constructor() {
        super();
    }
}
