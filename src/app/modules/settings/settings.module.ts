import {AbstractModule} from '$helpers/index';

import {SettingsService} from '$settings/services';

export class SettingsModule extends AbstractModule {
    public readonly moduleParams = {
        name: 'Settings',
        components: [],
        providers: [
            SettingsService,
        ],
    }

    constructor() {
        super();
    }
}
