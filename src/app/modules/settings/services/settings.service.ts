import {AbstractService} from '$helpers/index';
import {IServiceParams} from '$interfaces';

export class SettingsService extends AbstractService{
    public readonly serviceParams: IServiceParams = {
        name: 'SettingsService',
        inject: [],
    }

    constructor() {
        super();
    }
}
