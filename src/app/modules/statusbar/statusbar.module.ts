import {AbstractModule} from '$helpers/index';

import {StatusbarService} from '$statusbar/services';

export class StatusbarModule extends AbstractModule {
    public readonly moduleParams = {
        name: 'Statusbar',
        components: [],
        providers: [
            StatusbarService,
        ],
    }

    constructor() {
        super();
    }
}
