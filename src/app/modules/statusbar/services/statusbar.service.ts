import {AbstractService} from '$helpers/index';
import {IServiceParams} from '$interfaces';

export class StatusbarService extends AbstractService{
    public readonly serviceParams: IServiceParams = {
        name: 'StatusbarService',
        inject: [],
    }

    constructor() {
        super();
    }
}
