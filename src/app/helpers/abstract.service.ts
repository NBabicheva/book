import {IServiceParams} from '$interfaces';
import {Bootstrap} from '$modules/bootstrap';

export abstract class AbstractService {
    public readonly serviceParams: IServiceParams;
    protected $bootstrap: Bootstrap;

    protected constructor () {
    }

    public init(bootstrap: Bootstrap): void {
        this.$bootstrap = bootstrap;
        console.log(this.serviceParams);
        if (this.serviceParams.inject?.length) {
            this.serviceParams.inject.map((name: string): void => {
                const serviceInstance = bootstrap.getServiceInstance(name),
                serviceName = name.split('.')[1];
                const serviceNameLowerCase = `${serviceName[0].toLowerCase()}${serviceName.slice(1)}`;
                this[serviceNameLowerCase] = serviceInstance;
            });
        }
    }
}
