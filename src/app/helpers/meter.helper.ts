export interface IDefaultParams {
    count?: number;
    name?: string;
    context?: any;
}

type TFunc<T> = (...params: any[]) => T;

const defaultParams = {
    count: 3,
    name: '',
    context:  null
};

export function meter<T extends any>(func, meterParams: IDefaultParams = defaultParams): TFunc<T> {
    return (...params: any[]): T => {
        let resultFunc: T;
        const timerArray: number[] = [];

        while (timerArray.length < meterParams.count) {
            const timeStart: number = performance.now();
            resultFunc = func.apply(meterParams.context, params);
            const timeEnd = performance.now();
            timerArray.push((performance.now() - timeStart));
        }
        const medianValue: number[] = timerArray.sort((a, b) => a - b)

        let res;

        const half = medianValue.length / 2;

        if (medianValue.length % 2) {
            res = medianValue[Math.floor(half)].toFixed(3);
        } else {
            res = (medianValue[half] + medianValue[half - 1]) / 2;
        }
        console.log(`%c${meterParams.name || func.name} (count ${meterParams.count}): ${res}ms`, 'color:  #f5e942');

        return resultFunc;
    };
}
