import {meter} from '$helpers/meter.helper';

export class Iterations {
    public meterCount: number = 1;
    public arrayCount: number = 1e5;
    public meterArray: number[];

    constructor() {
        this.meterArray = Array(this.arrayCount).fill(1, 0, this.arrayCount)
    }

    public testMeterFor(): void {
        for (let i = 0; i < this.arrayCount; i++) {
            Math.random();
        }
    }

    public testMeterWhile(): void {
        let i = 0;
        while(i < this.arrayCount) {
            Math.random();
            i++;
        }
    }

    public testMeterMap(): void {
        this.meterArray.map(() => {
            Math.random();
        });
    }

    public testMeterEvery(): void {
        this.meterArray.every(() => {
            Math.random();
            return true;
        });
    }

    public testMeterForEach(): void {
        this.meterArray.forEach(() => {
            Math.random();
            return;
        });
    }

    public testMeterForOf(): void {
        for (const i of this.meterArray) {
            Math.random();
        }
    }

    public testMeterForIn(): void {
        for (const i in this.meterArray) {
            Math.random();
        }
    }
}

export class Iterations2 {
    public meterCount: number = 1;
    public arrayCount: number = 1000;

    constructor() {}

    public ipAddressCheck(ipAddress) {
        let sample = /([0-9]{1,3}[\.]){3}[0-9]{1,3}/;
        if(ipAddress.match(sample)) {
        return true;
        } else {
        alert("Please enter a valid ip Address.");
        return false;
        }
    };
}
